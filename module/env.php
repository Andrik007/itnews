<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE); 
define("SITENAME", "IT-NEWS");
define("LOGO", "/image/logo.png");
define("BASEURL", "http://it-news.tk");
define("E404", BASEURL."/404");
define("ESEC", BASEURL."/security_error");
$__DEF["header_links"] = array(
    "Новини" => "/news",
    "Статті" => "/articles",
    "Фастріди" => "/fastread",
    "Переклади" => "/translations",
    "Про проект" => "/about",
    "Пошук" => "/search"
);
$__DEF["header_links_admin"] = array(
    "Налаштування акаунта" => "/admin_account",
    "Вихід" => "/admin_logout"
);
$__DEF["footer_links"] = array(
    "Новини" => "/news",
    "Статті" => "/articles",
    "Фастріди" => "/fastread",
    "Переклади" => "/translations",
    "Про проект" => "/about",
    "Пошук" => "/search",
    "Адмінка" => "/login",
    "uncappteam@gmail.com" => "mailto:uncappteam@gmail.com"
);
