<?php
session_start();
require_once("module/env.php");
require_once("module/mysql.php");
function isaction(){
    if(!isset($_GET["action"])) return "index";
    else return $_GET["action"];
}
function ispaction(){
    if(!isset($_GET["postaction"])) return "index";
    else return $_GET["postaction"];
}
//var_dump($_GET);
//var_dump($_GET);
switch(isaction()){
    case "read":require_once("view/template_news.php");break;
    case "index":require_once("view/template_index.php");break;
    case "login":require_once("view/template_login.php");break;
    case "404":require_once("view/template_404.php");break;
    case "403":require_once("view/template_403.php");break;
    case "security_error":require_once("view/template_403.php");break;
    case "author":require_once("module/open-author.module.php");break;
    case "try":require_once("module/login-check.module.php");loadModule();break;
    case "admin":{
        switch(ispaction()){
            case "edit": require_once("view/template_admin_edit.php");break;
            case "index": require_once("view/template_admin.php");break;
            case "create": require_once("module/create-new.module.php");break;
            case "account": require_once("view/template_admin_account.php");break;
            case "logout": require_once("module/admin-logout.module.php");break;
            default: require_once("view/template_404.php");break;
        }
        break;
    }
    case "text_read":require_once("view/template_news_list.php");break;
    case "search":require_once("view/template_search.php");break;
    case "about":require_once("view/template_about.php");break;
    default:require_once("view/template_404.php");break;
}

$_mysqli -> close();
