<?php
require_once("./module/env.php");
require_once("./controller/admin-page-controller.php");
$_DESIGN=array(
    "title" => SITENAME,
    "image" => array(
        "logo" => LOGO
    ),
    "lib_add" => "<script src='/js/admin-list.js'></script>",
    "links" => $__DEF["header_links_admin"],
    "header-force" => "",
    "tid" => 4
);
function loadContent(){
    loadController();
    ?>
    <span class="hidden" id="q_type"><?=$_GET["what"];?></span>
    <div class="col-12 news-list" id="news-list">
        <h1 align=center>ВСІ <?=getThisTypeNamePlural($_GET["what"]);?></h1>
        <hr>
        <div class="element" id="add_text">
            <div class="left"><i class="material-icons">add_circle</i></div>
            <div class="right"><h2 align="center">Додати статтю...</h2></div>
        </div>
        <hr>
    </div>
    <?php
}
require_once("./template_main.php");
