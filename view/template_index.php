<?php
require_once("./module/env.php");
require_once("./controller/main-page-controller.php");
$_DESIGN=array(
    "title" => SITENAME,
    "image" => array(
        "logo" => LOGO
    ),
    "lib_add" => "",
    "links" => $__DEF["header_links"],
    "header-force" => "",
    "tid" => 1
);
function loadContent(){
    loadController();
    echo '</div><div class="bef-blocks">';
    echo "<h1 align=center>Головне</h1>";
    echo "<hr color=grey />";  
    echo '</div><div class="container">'; 
    ?>
    <div class="row">
        <div class="col-12 col-md-5 news-featured-block">
            <img src="<?php echo loadFeaturedImage(1);?>" alt="">
            <div class="data">
               <span class="header"><?php echo loadFeaturedName(1);?></span>
                <div class="preread"><?=getPrereadFeatured(1);?>...</div>
                <div class="readbuttoncnt"><span class="news-type" style="<?=getLastFeaturedColorType(1);?>"><?=getLastFeaturedType(1);?></span><button class="read btn btn-default" onclick="window.location.href='<?=getFull(1);?>'"><span class="text">Читати далі</span><i class="material-icons">navigate_next</i></button></div>
            </div>
        </div>
        <div class="col-12 col-md-5 offset-md-2 news-featured-block">
            <img src="<?php echo loadFeaturedImage(2);?>" alt="">
            <div class="data">
               <span class="header"><?php echo loadFeaturedName(2);?></span>
                <div class="preread"><?=getPrereadFeatured(2);?>...</div>
                <div class="readbuttoncnt"><span class="news-type" style="<?=getLastFeaturedColorType(2);?>"><?=getLastFeaturedType(2);?></span><button class="read btn btn-default" onclick="window.location.href='<?=getFull(2);?>'"><span class="text">Читати далі</span><i class="material-icons">navigate_next</i></button></div>
            </div>
        </div>
    </div>
    <?php
    echo '</div><div class="bef-blocks">';
    echo "<h1 align=center>Останні новини</h1>";
    echo "<hr color=grey />";  
    echo '</div><div class="news-cnt">'; 
    for($i=0;$i<getLNC();$i++){
        $this_news = getLastNewsByID($i);
        $text_to_edit = sprintf('
            <div class="news-block">
                <img src="%s" alt="">
                <div class="data">
                   <span class="header %s">%s</span>
                    <div class="preread">%s</div>
                    <div class="readbuttoncnt"><span class="news-type" style="%s">%s</span><button class="read btn btn-default" onclick="window.location.href=\'%s\'"><span class="text">Читати далі</span><i class="material-icons">navigate_next</i></button></div>
                </div>
            </div>', $this_news["preimageurl"], ($this_news["featured"]=="1"?"header-bold":""), $this_news["header"], $this_news["preread"], getLastColorType($i+1), getLastType($i+1), "/read?id=".$this_news["id"]);
        echo $text_to_edit;
    }
    echo '  <a href="/all" class="news-block readall">
                <img src="/image/read-all.svg" alt="">
                <span class="bg-text">ЧИТАТИ ВСІ</span>
            </a>';
    echo "</div>";
}
require_once("./template_main.php");
