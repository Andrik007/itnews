<?php
require_once("./module/env.php");
require_once("./controller/login-page-controller.php");
$_DESIGN=array(
    "title" => SITENAME,
    "image" => array(
        "logo" => LOGO
    ),
    "lib_add" => "",
    "links" => $__DEF["header_links"],
    "tid" => 3,
    "header-force" => "display:none !important;"
);
function loadContent(){
    loadController();
    ?>
    </div>
    <div class="login-box">
        <span class="alert1"><?=getAlert();?></span>
        <div class="block-skip" style="<?=getAlertAdd();?>"></div>
        <form action="/try" class="login-form" method="post">
            <div class="block1"><input type="text" class="login_input" value="<?=(isset($_GET['user']) ? $_GET['user'] : '')?>" id="login_input_1" name="i1" placeholder="ЛОГІН..."></div>
            <div class="block2"><input type="password" class="login_input" id="login_input_2" name="i2" placeholder="ПАРОЛЬ..."></div>
            <div class="block3"><button class="btn btn-primary" type="submit">Ввійти</button></div>
        </form>
    </div>
    <div>
    <?php
}
require_once("./template_main.php");
