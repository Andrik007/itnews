<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?=$_DESIGN["title"];?></title>
    <link href="https://fonts.googleapis.com/css?family=Exo+2:400,400i,500,500i&amp;subset=cyrillic,latin-ext" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet/less" type="text/css" href="/less/desktop.less" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/less.js/3.0.0/less.min.js" ></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="apple-touch-icon" sizes="57x57" href="/image/favicons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/image/favicons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/image/favicons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/image/favicons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/image/favicons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/image/favicons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/image/favicons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/image/favicons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/image/favicons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/image/favicons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/image/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/image/favicons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/image/favicons/favicon-16x16.png">
    <link rel="manifest" href="/image/favicons/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/image/favicons/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <?=$_DESIGN["lib_add_pre"];?>
    <script src="/js/main.js"></script>
    <?=$_DESIGN["lib_add"];?>
</head>
<body>
<header style="<?=$_DESIGN["header-force"];?>">
    <img src="<?=$_DESIGN["image"]["logo"];?>" class="logo_image"/>
    <nav>
        <?php
            foreach ($_DESIGN["links"] as $name => $url)
                echo "<a class=\"headerlink\" href=\"".$url."\">".$name."</a>";
        ?>
    </nav>
</header>
<div id="main_<?=$_DESIGN['tid'];?>">
  <div class="container">
       <?=loadContent();?>
   </div>
</div>
<footer>
    <div class="container">
        <div class="row">
            <img src="<?=$_DESIGN["image"]["logo"];?>" class="logo_image col-12 col-md-6"/>
            <div class="col-12 col-md-6 container-fluid cc">
                <?php
                    $c = 0;
                    foreach ($__DEF["footer_links"] as $name => $url)
                        echo ($c%2==0?"<div class='row'>":"")."<a class=\"footerlink col-md-6\" href=\"".$url."\">".$name."</a>".($c%2==0?"":"</div>");
                ?>
            </div>
        </div>
    </div>
</footer>
<?=$_DESIGN["lib_page_end"]?>
</body>
</html>
