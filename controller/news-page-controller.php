<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/module/env.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/module/mysql.php");
$th;
$tid;
function loadController($d){
    global $tid;
    getNewsId($d);
    $tid=$d;
}

function getNewsId($m){
    global $_mysqli, $th;
    if($res = $_mysqli -> query("SELECT * FROM `texts` WHERE `id` = $m AND `open` = '1'")){
        if($row = $res -> fetch_assoc())
            $th = $row;
        else
            header("Location: ".E404);
    }
    if(!isset($_SESSION["views"][$th["id"]])) {$_mysqli -> query("UPDATE `texts` SET `views` = ".($th["views"]+1)." WHERE `id` = ".$th["id"]);$_SESSION["views"][$th["id"]]=true;}
    
}
function getName(){
    global $th;
    return $th["header"];
}
function getViews(){
    global $th;
    return $th["views"];
}
function getTime(){
    global $th;
    return $th["pubtime"];
}
function getTime2(){
    global $th;
    return $th["readtime"];
}
function getSRCUrl(){
    global $th;
    return isset(json_decode($th["source"], true)["url"])?json_decode($th["source"], true)["url"]:"#";
}
function getSRCName(){
    global $th;
    return isset(json_decode($th["source"], true)["name"])?json_decode($th["source"], true)["name"]:"Відсутнє";
}
function getAuthorName(){
    global $th;
    return getAuthorUrlAndNameById($th["author"])["name"];
}
function getAuthorUrl(){
    global $th;
    return BASEURL."/?action=author&id=".$th["author"];
}
function getAuthorUrlAndNameById($id){
    global $_mysqli;
    if($res = $_mysqli -> query("SELECT * FROM `authors` WHERE `id` = $id")){
        while($row = $res -> fetch_assoc()) return $row;
    }
}
function getImageWithID($m){
    global $tid;
    return "/image/texts/".$tid."_".$m.".jpg";
}
function getTextHTML(){
    global $th;
    return $th["text_html"];
}
function getTextAMP(){
    global $th;
    return $th["text_amp"];
}
