<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/module/env.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/module/mysql.php");
$last_news; $lnc = 0;
$last_featured_news; $lfnc = 0;
function loadController(){
    getLastNews(20);
    getLastFeaturedNews(2);
}
function loadFeaturedImage($i){
    global $last_featured_news, $lfnc;
    return $last_featured_news[min($i-1, $lfnc-1)]["preimageurl"];
}
function getLNC(){
    global $lnc;
    return $lnc;
}
function getLastNewsByID($i){
    global $last_news;
    return $last_news[$i];
}
function getLastFeaturedType($i){
    global $last_featured_news, $lfnc;
    $type = $last_featured_news[min($i-1, $lfnc-1)]["pubtype"];
    switch($type){
        case "NEWS": return "НОВИНИ";break;
        case "ARTICLE": return "СТАТТЯ";break;
        case "FASTREAD": return "ФАСТРІД";break;
        case "TRANSLATION": return "ПЕРЕКЛАД";break;
    }
}
function getLastFeaturedColorType($i){
    global $last_featured_news, $lfnc;
    $type = $last_featured_news[min($i-1, $lfnc-1)]["pubtype"];
    switch($type){
        case "NEWS": return "background-color:rgba(255, 0, 0, 0.75);";break;
        case "ARTICLE": return "background-color:rgba(0, 255, 0, 0.75);";break;
        case "FASTREAD": return "background-color:rgba(0, 0, 255, 0.75);";break;
        case "TRANSLATION": return "background-color:rgba(255, 0, 255, 0.75);";break;
    }
}
function getLastType($i){
    global $last_news, $lnc;
    $type = $last_news[min($i-1, $lnc-1)]["pubtype"];
    switch($type){
        case "NEWS": return "НОВИНИ";break;
        case "ARTICLE": return "СТАТТЯ";break;
        case "FASTREAD": return "ФАСТРІД";break;
        case "TRANSLATION": return "ПЕРЕКЛАД";break;
    }
}
function getLastColorType($i){
    global $last_news, $lnc;
    $type = $last_news[min($i-1, $lnc-1)]["pubtype"];
    switch($type){
        case "NEWS": return "background-color:rgba(255, 0, 0, 0.75);";break;
        case "ARTICLE": return "background-color:rgba(0, 255, 0, 0.75);";break;
        case "FASTREAD": return "background-color:rgba(0, 0, 255, 0.75);";break;
        case "TRANSLATION": return "background-color:rgba(255, 0, 255, 0.75);";break;
    }
}
function loadFeaturedName($i){
    global $last_featured_news, $lfnc;
    return $last_featured_news[min($i-1, $lfnc-1)]["header"];
}
function getViewsFeatured($i){
    global $last_featured_news, $lfnc;
    return $last_featured_news[min($i-1, $lfnc-1)]["views"];
}
function getTimeFeatured($i){
    global $last_featured_news, $lfnc;
    return $last_featured_news[min($i-1, $lfnc-1)]["readtime"];
}
function getAuthorNameFeatured($i){
    global $last_featured_news, $lfnc;
    return getAuthorUrlAndNameById($last_featured_news[min($i-1, $lfnc-1)]["author"])["name"];
}
function getAuthorUrlFeatured($i){
    global $last_featured_news, $lfnc;
    return BASEURL."/author?id=".$last_featured_news[min($i-1, $lfnc-1)]["author"];
}
function getPrereadFeatured($i){
    global $last_featured_news, $lfnc;
    return $last_featured_news[min($i-1, $lfnc-1)]["preread"];
}
function getFull($i){
    global $last_featured_news, $lfnc;
    return BASEURL."/read?id=".$last_featured_news[min($i-1, $lfnc-1)]["id"];
}
function getLastNews($m){
    global $_mysqli, $last_news, $lnc;
    if($res = $_mysqli -> query("SELECT * FROM `texts` WHERE `open` = '1' ORDER BY `pubtime` DESC LIMIT $m")){
        while($row = $res -> fetch_assoc()) $last_news[$lnc++] = $row;
    }
}
function getLastFeaturedNews($m){
    global $_mysqli, $last_featured_news, $lfnc;
    if($res = $_mysqli -> query("SELECT * FROM `texts` WHERE `featured` = 1 AND `open` = '1' ORDER BY `pubtime` DESC LIMIT $m")){
        while($row = $res -> fetch_assoc()) $last_featured_news[$lfnc++] = $row;
    }
}
function getAuthorUrlAndNameById($id){
    global $_mysqli;
    if($res = $_mysqli -> query("SELECT * FROM `authors` WHERE `id` = $id")){
        while($row = $res -> fetch_assoc()) return $row;
    }
}
?>
